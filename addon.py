import sys
import xbmc
import xbmcgui
import xbmcplugin
try:
    import simplejson as json
except ImportError:
    import json

pluginHandle = int(sys.argv[1])
isAddItemSuccess = True

def addToNextUnwatchedPlaylist(tvshow):
    global pluginHandle
    global isAddItemSuccess
    fetchTvEpisodeCommand = json.dumps({
        'jsonrpc': '2.0',
        'method': 'VideoLibrary.GetEpisodes',
        'params': {
            'tvshowid': tvshow['tvshowid'],
            'properties': ['title', 'playcount', 'file', 'thumbnail', 'tvshowid', 'season', 'episode', 'showtitle', 'plot'],
            'filter': {'field': 'playcount', 'operator': 'is', 'value': '0'},
            'limits': {'start': 0, 'end': 1},
            'sort': {"method": "label"}
        },
        'id': pluginHandle
    })
    episodes = xbmc.executeJSONRPC(fetchTvEpisodeCommand)
    episodes = json.loads(episodes)
    if 'result' in episodes and 'episodes' in episodes['result']:
        episodes = episodes['result']['episodes']
        if len(episodes) > 0:
            episode = episodes[0]
            item = xbmcgui.ListItem(tvshow['label'] + ' ' + episode['label'], thumbnailImage=episode['thumbnail'], iconImage='DefaultVideo.png')
            item.setInfo('video', episode)
            isAddThisItemSuccess = xbmcplugin.addDirectoryItem(handle=pluginHandle, url=episode['file'], listitem=item, isFolder=False)
            isAddItemSuccess = isAddItemSuccess and isAddThisItemSuccess


xbmcplugin.setContent(pluginHandle, 'episodes')

fetchTvShowCommand = json.dumps({
    'jsonrpc': '2.0',
    'method': 'VideoLibrary.GetTVShows',
    'params': {
        'properties': ['title', 'episode', 'watchedepisodes'],
        'limits': {'start': 0, 'end': 200},
        'sort': {'method': 'lastplayed', 'order': 'descending'}
    },
    'id': pluginHandle
})
tvshows = xbmc.executeJSONRPC(fetchTvShowCommand)
tvshows = json.loads(tvshows)
if 'result' in tvshows and 'tvshows' in tvshows['result']:
    tvshows = tvshows['result']['tvshows']
    for tvshow in tvshows:
        if tvshow['watchedepisodes'] < tvshow['episode']:
            print 'tvshow=' + str(tvshow)
            addToNextUnwatchedPlaylist(tvshow)

xbmcplugin.endOfDirectory(pluginHandle, isAddItemSuccess, cacheToDisc=False)